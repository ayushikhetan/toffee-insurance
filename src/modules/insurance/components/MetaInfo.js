import React from "react";
import { Row, Col, Icon } from "antd";
import { css } from "emotion";

const Contianer = css`
  i {
    font-size: 18px;
    margin-top: 4px;
  }
`;

const MetaInfo = props => (
  <Row className={Contianer}>
    <Col xs={2}>
      <Icon type="question-circle" theme="outlined" />
    </Col>
    <Col xs={22}>
      <p>{props.children}</p>
    </Col>
  </Row>
);

export default MetaInfo;
