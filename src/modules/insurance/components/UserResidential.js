import React, { Component } from "react";
import { Row, Col, Radio, Form, Icon, message } from "antd";
import Layout from "./Layout";
import Styles from "./UserResidential.css";
import UserDetailsForm from "./UserDetailsForm";
import ToffeeButton from "./ToffeeButton";
import CustomerCareExecutive from "./CustomerCareExecutive";
import MetaInfo from "./MetaInfo";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateUser } from "../insuranceActions";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class UserResidential extends Component {
  state = {
    firstName: null,
    email: null,
    mobileNumber: null,
    getFieldDecorator: null,
    userTitle: "Mr"
  };
  validateAndGotoDOB = () => {
    const { firstName, email, mobileNumber, userTitle } = this.state;

    if (!firstName) {
      return message.warn("Please provide your first name");
    }

    if (!email || !this.isEmailValid(email)) {
      return message.warn("Please provide a valid email");
    }

    if (!mobileNumber || !this.isMobileValid(mobileNumber)) {
      return message.warn("Please provide a valid mobile number");
    }

    this.props.updateUser({ firstName, email, mobileNumber, userTitle });
    return this.props.history.push("/user-dob");
  };

  updateFieldValue = (field, value) => {
    this.setState({
      [field]: value
    });
  };

  // TODO: Move this to common/utils
  isEmailValid(email) {
    // eslint-disable-next-line no-useless-escape
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  // TODO: Move this to common/utils
  isMobileValid(mobileNumber) {
    if (typeof mobileNumber === "string" && mobileNumber.length === 10) {
      return true;
    }
    return false;
  }

  render() {
    const { firstName, email, mobileNumber, userTitle } = this.state;
    return (
      <Row className={Styles.contianer}>
        <CustomerCareExecutive>
          Hi! I am Nishi, one of the certified insurance professionals at
          Toffee. Thanks for choosing Dengue Insurance. Enter details of the
          person to be insured as per Aadhaar.
        </CustomerCareExecutive>
        <Col xs={24} className={Styles.userDetails}>
          <h2>Details</h2>
          <RadioGroup
            onChange={e => this.updateFieldValue("userTitle", e.target.value)}
            defaultValue={"Mr"}
            value={userTitle}
            className="my-radio"
          >
            <RadioButton value="Mr">Mr</RadioButton>
            <RadioButton value="Ms">Ms</RadioButton>
            <RadioButton value="Mrs">Mrs</RadioButton>
          </RadioGroup>
          <Form>
            <UserDetailsForm
              firstName={firstName}
              email={email}
              mobileNumber={mobileNumber}
              updateFieldValue={this.updateFieldValue}
            />
          </Form>
        </Col>
        <Col xs={24}>
          <MetaInfo>
            If the insured person doesn't have email and/or mobile, please enter
            that of the buyer.
          </MetaInfo>
        </Col>
        <Col xs={24} className={Styles.buttonContainer}>
          <ToffeeButton
            onClick={this.validateAndGotoDOB}
            type="primary"
            size="large"
          >
            Next
            {"  "}
            <Icon type="arrow-right" theme="outlined" />
          </ToffeeButton>
        </Col>
      </Row>
    );
  }
}

export { UserResidential };

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateUser }, dispatch);
};

const ConnectedUserResidential = connect(
  null,
  mapDispatchToProps
)(UserResidential);

export default props => (
  <Layout>
    <ConnectedUserResidential {...props} />
  </Layout>
);
