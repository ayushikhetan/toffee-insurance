import { css } from "emotion";

const Styles = {
  contianer: css`
    padding: 4% 8%;
    background-color: #fbfcfd;
    padding-bottom: 20%;
  `,
  userDetails: css`
    h2 {
      font-size: 22px;
      font-weight: 300;
      color: #22334f;
    }
    .ant-radio-button-wrapper {
      border-radius: 50%;
      width: 40px;
      height: 40px;
      line-height: 2.5;
      text-align: center;
      margin-right: 8px;
      padding: 0;
      opacity: 1;
      box-shadow: 0 1px 5px 0 rgba(99, 135, 170, 0.06);
      cursor: pointer;

      border: solid 2px rgba(34, 51, 79, 0.08);
      color: #cdd1d7;

      &:before {
        display: none;
      }
    }
    .ant-radio-button-wrapper.ant-radio-button-wrapper-checked {
      border: 2px solid rgba(106, 225, 231, 0.75);
      color: #22334f;
    }
    form {
      margin: 8% 0;
    }
    .ant-form-item {
      margin-bottom: 0;
    }
  `,
  buttonContainer: css`
    text-align: center;
  `
};

export default Styles;
