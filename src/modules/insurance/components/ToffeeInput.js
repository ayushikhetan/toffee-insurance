import React from "react";
import { Input } from "antd";
import { css } from "emotion";

const ToffeeInputStyle = css`
  border: none;
  box-shadow: 0 4px 5px 0 rgba(99, 135, 170, 0.14);
  font-size: 23px;
  height: 65px;
  font-size: 17px;
  font-weight: 300;
  padding-left: 15px;
  padding-left: 20px;
  border: solid 1px rgba(239, 245, 252, 0.75);
`;

const ToffeeInput = props => <Input className={ToffeeInputStyle} {...props} />;

export default ToffeeInput;
