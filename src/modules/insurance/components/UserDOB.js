import React, { Component } from "react";
import { Row, Col, Icon, message } from "antd";
import CustomerCareExecutive from "./CustomerCareExecutive";
import MetaInfo from "./MetaInfo";
import UserInsuranceCard from "./UserInsuranceCard";
import ToffeeButton from "./ToffeeButton";
import ToffeeInput from "./ToffeeInput";
import Layout from "./Layout";
import Styles from "./UserDOB.css";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateUser } from "../insuranceActions";

class UserDOB extends Component {
  state = {
    dob: null
  };
  updateDOB = e => {
    const value = e.target.value;
    this.setState({
      dob: value
    });
  };
  validateAndUpdateUser = () => {
    const { dob } = this.state;
    if (!dob) {
      return message.warn("Please provide a valid date of birth");
    }
    this.props.updateUser({ dob });
    return message.success(
      "You have successfully applied for a toffee insurance"
    );
  };
  render() {
    const { user } = this.props;
    const { dob } = this.state;
    return (
      <Row className={Styles.container}>
        <Col xs={24}>
          <UserInsuranceCard user={user} className="a" />
        </Col>
        <Col xs={24} className={Styles.ccsSection}>
          <CustomerCareExecutive>
            OK great! Now please enter the Date of Birth of the person to be
            insured as per Aadhaar.
          </CustomerCareExecutive>
        </Col>
        <Col xs={24} className={Styles.headingSection}>
          <h2>
            <Icon type="shopping" theme="outlined" />
            Date of Birth
          </h2>
        </Col>
        <Col xs={24} className={Styles.inputSection}>
          <ToffeeInput
            onChange={this.updateDOB}
            value={dob}
            placeholder="dd/mm/yy"
          />
        </Col>
        <Col xs={24} className={Styles.metaSection}>
          <MetaInfo>
            Insured person should be between 18 to 64 years old to buy this
            Toffee.
          </MetaInfo>
        </Col>
        <Col xs={24} className={Styles.buttonSection}>
          <ToffeeButton onClick={this.validateAndUpdateUser}>
            Next
            {"  "}
            <Icon type="arrow-right" theme="outlined" />
          </ToffeeButton>
        </Col>
      </Row>
    );
  }
}

export { UserDOB };

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ updateUser }, dispatch);
};

const ConnectedUserDOB = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDOB);

export default props => (
  <Layout>
    <ConnectedUserDOB {...props} />
  </Layout>
);
