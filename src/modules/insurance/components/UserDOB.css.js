import { css } from "emotion";

const BOTTOM_SECTION_PADDING = "padding: 0 4%";

const Styles = {
  container: css`
    padding: 4%;
    background-color: #fbfcfd;
    padding-bottom: 20%;
  `,
  ccsSection: css`
    margin: 8% 0 6% 0;
    padding: 0 4%;
  `,
  headingSection: css`
    ${BOTTOM_SECTION_PADDING};
    h2 {
      font-size: 19px;
      font-weight: 300;
      letter-spacing: -0.6px;
      color: #22334f;
    }
    i {
      margin-right: 5%;
    }
  `,
  inputSection: css`
    ${BOTTOM_SECTION_PADDING};
    margin-bottom: 6%;
  `,
  metaSection: css`
    ${BOTTOM_SECTION_PADDING};
  `,
  buttonSection: css`
    ${BOTTOM_SECTION_PADDING};
    margin-top: 5%;
  `
};

export default Styles;
