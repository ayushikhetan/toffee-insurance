import React from "react";
import { Button } from "antd";
import { css } from "emotion";

const ToffeeButtonStyle = css`
  border-radius: 0;
  background-image: linear-gradient(242deg, #eb5757, #e64b4b);
  border-color: #e64b4b;
  width: 100%;
  height: 66px;
  color: white;
  font-size: 20px;
  &:hover {
    background-image: linear-gradient(242deg, #eb5757, #e64b4b);
    border-color: #e64b4b;
    opacity: 0.8;
  }
`;

const ToffeeButton = props => (
  <Button className={ToffeeButtonStyle} {...props}>
    {props.children}
  </Button>
);

export default ToffeeButton;
