import { css } from "emotion";

const Styles = {
  headerContainer: css`
    padding: 8px 20px;
    background: white;
    z-index: 11;

    i {
      font-size: 20px;
    }
  `
};

export default Styles;
