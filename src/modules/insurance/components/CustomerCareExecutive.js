import React, { Fragment } from "react";
import { Col } from "antd";
import { css } from "emotion";

const cceImage = css`
  width: 35px;
  border-radius: 50%;
`;

const cceText = css`
  color: #22334f;
`;

const CustomerCareExecutive = props => (
  <Fragment>
    <Col xs={4}>
      <img
        className={cceImage}
        src="https://toffeeinsurance.com/images/xl-bot.png"
        alt="insurance"
      />
    </Col>
    <Col xs={20}>
      <p className={cceText}>{props.children}</p>
    </Col>
  </Fragment>
);

export default CustomerCareExecutive;
