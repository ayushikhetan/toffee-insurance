import React, { Component } from "react";
import { Row, Affix, Icon } from "antd";
import Styles from "./Header.css";

class Header extends Component {
  render() {
    return (
      <Affix offsetTop={1}>
        <Row
          className={Styles.headerContainer}
          type="flex"
          justify="space-between"
          align="middle"
        >
          <Icon type="arrow-left" theme="outlined" />
          <img
            src="https://toffeeinsurance.com/images/toffee-logo.svg"
            alt="logo"
          />
          <Icon type="phone" theme="filled" />
        </Row>
      </Affix>
    );
  }
}

export default Header;
