import React, { Fragment } from "react";
import ToffeeInput from "./ToffeeInput";

const UserDetailsForm = props => {
  return (
    <Fragment>
      {/** Name Field */}
      <ToffeeInput
        value={props.firstName}
        onChange={e => props.updateFieldValue("firstName", e.target.value)}
        placeholder="Full Name"
      />

      {/** Email Field */}
      <ToffeeInput
        value={props.email}
        onChange={e => props.updateFieldValue("email", e.target.value)}
        placeholder="Email"
      />

      {/** 10 digit mobile number Field */}
      <ToffeeInput
        value={props.mobileNumber}
        onChange={e => props.updateFieldValue("mobileNumber", e.target.value)}
        placeholder="10-digit Mobile Number"
      />
    </Fragment>
  );
};

export default UserDetailsForm;
