import React, { Component } from "react";
import { Row, Col } from "antd";
import Styles from "./Policies.css";
import Layout from "./Layout";
import ToffeeButton from "./ToffeeButton";
import {Link} from "react-router-dom"

class Policies extends Component {
  render() {
    return (
      <Row className={Styles.contianer}>
        <Col className={Styles.productContainer} xs={24}>
          <h2>Dengue Insurance</h2>
          <p>Health insurance to cover all health related expenses</p>

          <Row className={Styles.productInfo}>
            <Col xs={12}>
              <h2>₹ 425</h2>
              <p>Annual Premium</p>
            </Col>
            <Col xs={12}>
              <h2>1 Year</h2>
              <p>Policy Duration</p>
            </Col>
          </Row>
        </Col>
        <Col xs={24} className={Styles.productImage}>
          <img
            src="https://toffeeinsurance.com/images/anti-dengue-toffee.svg"
            alt="dengue-insurance"
          />
        </Col>
        <Col xs={24} className={Styles.productFeaturesContainer}>
          <h2>What is covered?</h2>
          <Row gutter={8} className={Styles.productFeatures}>
            <Col xs={12}>
              <div>
                <h2>Hospitalisation</h2>
                <p>Upto ₹ 1,00,00</p>
              </div>
            </Col>
            <Col xs={12}>
              <div>
                <h2>Diagnostic Test</h2>
                <p>Covered</p>
              </div>
            </Col>
            <Col xs={12}>
              <div>
                <h2>Medicies</h2>
                <p>Covered</p>
              </div>
            </Col>
            <Col xs={12}>
              <div>
                <h2>Diagnostic Test</h2>
                <p>Covered</p>
              </div>
            </Col>
          </Row>
        </Col>
        <Col xs={24} className={Styles.coverageButton}>
          <Link to='/user-residential'>
            <ToffeeButton type="primary" size="large">
              See converage details
            </ToffeeButton>
          </Link>
        </Col>
      </Row>
    );
  }
}

export { Policies };

export default props => (
  <Layout>
    <Policies {...props} />
  </Layout>
);
