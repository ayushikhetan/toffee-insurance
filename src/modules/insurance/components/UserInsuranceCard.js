import React from "react";
import { Card, Row, Col } from "antd";
import { css } from "emotion";

const Container = css`
  border-radius: 8px;
  .ant-card-body {
    padding: 0;
    box-shadow: 0 0 16px 0 #c3eba6;
  }
`;

const ContentContianer = css`
  border-radius: 8px;
  color: white;
  background-image: linear-gradient(240deg, #54af09, #7db10b);
`;

const TopSection = css`
  padding: 15px 25px 0 25px;

  h2 {
    font-size: 18px;
    margin-bottom: 4px;
    line-height: 1;
    color: inherit;
    font-weight: 400;
  }
  p {
    font-size: 11px;
    opacity: 0.7;
  }
  img {
    width: 50px;
    filter: brightness(0) invert(1);
  }
`;

const MiddleSection = css`
  padding: 15px 25px 0 25px;
  background: rgba(255, 255, 255, 0.1);

  h2 {
    opacity: 0.9;
    font-size: 8px;
    font-weight: 300;
    letter-spacing: 0.4px;
    color: #ffffff;
    margin-bottom: 0;
  }

  p {
    opacity: 0.4;
    font-size: 22px;
    letter-spacing: 1.3px;
    margin-bottom: 0;
  }
`;

const BottomSection = css`
  padding: 15px 25px 0 25px;

  h2 {
    font-size: 18px;
    font-weight: 300;
    letter-spacing: -0.5px;
    text-transform: capitalize;
    color: inherit;
    margin-bottom: 0;
  }
  p {
    opacity: 0.7;
    font-size: 10px;
    letter-spacing: 0.1px;
  }
  img {
    width: 74px;
    opacity: 0.5;
  }
`;

const UserInsuranceCard = props => (
  <Card className={Container} hoverable={true}>
    <Row className={ContentContianer}>
      <Col xs={24}>
        <Row className={TopSection}>
          <Col xs={18}>
            <h2>Dengue Insurance</h2>
            <p>Sep 16 2018 - Sep 17, 2018</p>
          </Col>
          <Col xs={6}>
            <img
              src="https://toffeeinsurance.com/images/toffee-logo.svg"
              alt="logo"
            />
          </Col>
        </Row>
      </Col>
      <Col xs={24} className={MiddleSection}>
        <h2>Proposal Number</h2>
        <p>- - - -</p>
      </Col>
      <Col xs={24} className={BottomSection}>
        <Row>
          <Col xs={18}>
            <h2>{props.user.firstName}</h2>
            <p>{props.user.email}</p>
          </Col>
          <Col xs={6}>
            <img
              src="https://toffeeinsurance.com/images/xl-apollo-munich.png"
              alt="logo"
            />
          </Col>
        </Row>
      </Col>
    </Row>
  </Card>
);

export default UserInsuranceCard;
