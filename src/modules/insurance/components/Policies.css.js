import { css } from "emotion";

const Styles = {
  contianer: css`
    text-align: center;
  `,
  productContainer: css`
    padding: 4% 14%;
    background-image: linear-gradient(240deg, #54af09, #7db10b);
    color: white;

    h2 {
      color: inherit;
      font-weight: bold;
      margin-bottom: 2px;
    }

    p {
      opacity: 0.8;
      line-height: 1.27;
    }
  `,
  productInfo: css`
    h2 {
      font-size: 19px;
    }
    p {
      font-size: 12px;
    }
  `,
  productImage: css`
    padding: 4% 0;

    img {
      width: 115px;
    }
  `,
  productFeaturesContainer: css`
    h2 {
      font-size: 20px;
      font-weight: 300;
      opacity: 0.8;
      color: #22334f;
    }
  `,
  productFeatures: css`
    padding: 2% 8%;
    > div {
      margin-bottom: 8px;
    }
    > div > div {
      padding: 10px 0 8px 0;
      border-radius: 2px;
      border: solid 1px #e1ebf3;
      min-height: 56px;
    }
    h2 {
      font-size: 16px;
      margin-bottom: 0;
      color: #22334f;
      font-weight: 400;
    }

    p {
      font-size: 12px;
      color: #22334f;
      opacity: 0.8;
      margin-bottom: 0;
    }
  `,
  coverageButton: css`
    padding: 5%;
    /* button {
      border-radius: 0;
      background-image: linear-gradient(242deg, #eb5757, #e64b4b);
      border-color: #e64b4b;
      &:hover {
        background-image: linear-gradient(242deg, #eb5757, #e64b4b);
        border-color: #e64b4b;
        opacity: 0.8;
      }
    } */
  `
};

export default Styles;
