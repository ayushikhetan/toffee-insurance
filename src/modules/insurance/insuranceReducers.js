import { UDPATE_USER } from "./insuranceActionTypes";

export const user = (state = {}, { type, payload }) => {
  switch (type) {
    case UDPATE_USER:
      return {
        ...state,
        ...payload
      };
    default:
      return state;
  }
};
