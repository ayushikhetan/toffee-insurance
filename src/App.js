import React, { Component } from "react";
import Policies from "./modules/insurance/components/Policies";
import UserResidential from "./modules/insurance/components/UserResidential";
import UserDOB from "./modules/insurance/components/UserDOB";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Policies} />
            <Route exact path="/user-dob" component={UserDOB} />
            <Route exact path="/user-residential" component={UserResidential} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
