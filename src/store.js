import { createStore, combineReducers, applyMiddleware, compose } from "redux";

import * as insurance from "./modules/insurance/insuranceReducers";

const rootReducer = combineReducers({
  ...insurance
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware()));

export default store;
